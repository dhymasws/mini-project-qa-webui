//import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
//import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
//import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
//import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//
//import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
//import com.kms.katalon.core.model.FailureHandling as FailureHandling
//import com.kms.katalon.core.testcase.TestCase as TestCase
//import com.kms.katalon.core.testdata.TestData as TestData
//import com.kms.katalon.core.testobject.TestObject as TestObject
//
//import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
//import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
//
//import internal.GlobalVariable
//import org.openqa.selenium.JavascriptExecutor
//import com.kms.katalon.core.annotation.BeforeTestCase
//import com.kms.katalon.core.annotation.BeforeTestSuite
//import com.kms.katalon.core.annotation.AfterTestCase
//import com.kms.katalon.core.annotation.AfterTestSuite
//import com.kms.katalon.core.context.TestCaseContext
//import com.kms.katalon.core.context.TestSuiteContext
//
//class WebTestListener {
//	@BeforeTestCase
//	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
//		 RunConfiguration.setDriverPreferencesProperty("Remote", "name",testCaseContext.getTestCaseId())
//	}
//
//	@AfterTestCase
//	  def sampleAfterTestCase(TestCaseContext testCaseContext) {
//	
//			web
//			String status = testCaseContext.getTestCaseStatus();
//			String reason = testCaseContext.getTestCaseStatus();
//			  def driver = DriverFactory.getWebDriver()
//			  JavascriptExecutor jse = (JavascriptExecutor)driver;
//			  jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \""+status+"\", \"reason\": \""+reason+"\"}}");
//			  WebUI.closeBrowser()
//	  }
//
//	@BeforeTestSuite
//	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
//		 RunConfiguration.setDriverPreferencesProperty("Remote","build",testSuiteContext.getTestSuiteId())
//	}
//
//   @AfterTestSuite
//   def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
//	println testSuiteContext.getTestSuiteId()
//   }
//
//}