#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login
Feature: Title of your feature
  I want to use this template for my feature file

  @login-success
  Scenario Outline: Title of your scenario outline
    Given I want to open login page
    When I type email <email> and <password>
    And I click login
    Then I see homepage
    Examples: 
      | email | password |
      | mangalex@email.com | 123123123 | 
     
  