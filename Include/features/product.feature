#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@product
Feature: Title of your feature
  I want to use this template for my feature file

  @product-buy
  Scenario Outline: After login, i wanna buy some item
    Given I need login
    When I input field <email> <password>
    And click login
    When I choose item
    And I click cart icon
    Then I click pay and see my transaction
        Examples: 
      | email | password |
      | mangalex@email.com | 123123123 | 
      
   @product-detail
   Scenario Outline: I wanna see detail item
    Given I need login first
    When I ipt field <email> and <password>
    And click login button
    Then I click detail and see detail item
         Examples: 
      | email | password |
      | mangalex@email.com | 123123123 | 